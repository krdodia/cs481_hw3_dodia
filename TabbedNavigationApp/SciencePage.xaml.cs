﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TabbedNavigationApp
{
    public partial class SciencePage : ContentPage
    {
        public SciencePage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(SciencePage)}; ctor");
        }
        void OnAppearing(object Sender, System.EventArgs eventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }
        void OnDisappearing(object Sender, System.EventArgs eventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
